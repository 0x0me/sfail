# #SBahnFail! - S-Bahn Berlin Failures
## About
 #SBahnFail! aggregates tweets related to (mostly) outages and resumption
messages related to the Berliner S-Bahn from different users into 
one view. It is more a proof of concept and learning application for
using [JQuery Mobile](http://jquerymobile.com/) and [AngularJS](http://angularjs.org/). 

Sources:

* @[s_bahn_berlin](https://twitter.com/s_bahn_berlin)
* @[sbahnberlin](https://twitter.com/SBahnBerlin)

## Licence

This file is part of #SBahnFail!.

 #SBahnFail! is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 #SBahnFail! is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with #SBahnFail!.  If not, see <http://www.gnu.org/licenses/>. 

