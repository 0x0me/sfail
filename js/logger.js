/*
 * (C) 2013 Michael Engelhardt <me@mindcrime-ilab.de>
 *
 * This file is part of #SBahnFail!.
 *
 * #SBahnFail! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * #SBahnFail! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with #SBahnFail!.  If not, see <http://www.gnu.org/licenses/>.
 */

var Util = Util || {};

/**
 * very simple and incomplete replacement for the console api for browser which are not supporting that one.
 */
Util.Logger = (function(lvl) {
	var level = lvl || Util.Logger.DEFAULT;

	/** define a browser dependent delegating proxy */
	var logger = !(window.console && console.log) ? {
		debug : function(dbg) {
		},
		log : function(dbg) {
		},
		error : function(dbg) {
		}
	} : console;
	/**
	 *
	 * @param {Object} dbg
	 */
	function _debug(dbg) {
		if (level >= Util.Logger.DEBUG) {
			return logger.debug(dbg);
		} else {
            return;
        }
	};
	function _log(dbg) {
		return logger.log(dbg);
	};
	function _error(dbg) {
		return logger.error(dbg);
	};
	return {
		debug : _debug,
		log : _log,
		error : _error
	}
});

Util.Logger.DEBUG = 2000;
Util.Logger.DEFAULT = 1000;
Util.Logger.ERROR = 500;
Util.Logger.QUIET = -1;
