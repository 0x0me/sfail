/*
 * (C) 2013 Michael Engelhardt <me@mindcrime-ilab.de>
 *
 * This file is part of #SBahnFail!.
 *
 * #SBahnFail! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * #SBahnFail! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with #SBahnFail!.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *
 * Data Provider
 */
angular.module("SFail", [], function(){}).factory("tweetService", function ($http, $templateCache) {
    var logger = new Util.Logger(Util.Logger.ERROR);
    var tweetService = {
        async : function (query) {
            var promise = $http.jsonp("https://search.twitter.com/search.json?callback=JSON_CALLBACK&q=" + encodeURIComponent(query), {cache: $templateCache}).then(function (response) {
                logger.debug("TweetService: Got Response [" + response + "]");
                return response.data;
            });

            return promise;
        }
    };
    return tweetService;
});



/**
 * SFail
 */
var SFail = SFail || {};


SFail.TweetListController = function (tweetService, $scope) {
    //-----------------------------------------------------------------------
    // Properties
    //-----------------------------------------------------------------------
    /**
     * List of providers; search queries to execute against the twitter API
     */
    var providers = ["from:s_bahn_berlin", "from:sbahnberlin"];

    /**
     * Logger instance
     */
    var logger = new Util.Logger(Util.Logger.ERROR);

    /*
     * holds number of sources to query and gets decremented if one source was
     * loaded. Used to determine if the loading process is completed to bind actions on it.
     */
    var loadCount = providers.length;

    //-----------------------------------------------------------------------
    // Starting code
    //-----------------------------------------------------------------------
    load();

    //-----------------------------------------------------------------------
    // Method definitions
    //-----------------------------------------------------------------------

    /**
     * Methods triggers the loading of data from the different twitter sources.
     */
    function load() {
        onBeforeLoad();
        angular.forEach(providers, function (value, key) {
            var pr = tweetService.async(value);
            pr.then(processTweets,error);
        });


    }

    /**
     * @param {Object} data
     * @param {Object} status
     */
    function processTweets(data, status) {
        logger.debug("processTweets(data, status): called with status " + status);
        $scope.status = status;
        if (typeof ($scope.tweets) === "undefined") {
            logger.debug("tweets is empty");
            var mapped = data.results.map(mapDate);
            $scope.tweets = mapped;
            $scope.filteredTweets = mapped.filter(filterFunc);
        } else {
            logger.debug("There are already some tweets [" + $scope.tweets.length + "]");
            var mapped = data.results.map(mapDate);
            $scope.tweets = $scope.tweets.concat(mapped).sort(sortFunc);
            $scope.filteredTweets = $scope.filteredTweets.concat(mapped.filter(filterFunc)).sort(sortFunc);
        }
        $scope.next = data.next_page;
        $scope.lastRefresh = Date.now();

        loadCount--;
        if (loadCount <= 0) {
            onLoadFinished();
        }
    }

    /**
     *
     * @param {Object} data
     * @param {Object} status
     */
    function error(data, status) {
        logger.debug("error(data, status): called with status " + status);
        $scope.error = data || "Request failed";
        $scope.status = status;
    }

    /**
     * Sort tweets by creation date in descending order.
     * @param {Object} a
     * @param {Object} b
     */
    function sortFunc(a, b) {
        return b.created_at - a.created_at;
    }

    /**
     * Filter all tweets which have no reference to public transport id (eg. S1 or S85)
     * @param {Object} elem
     */
    function filterFunc(elem) {
        logger.debug("==>" + elem.text);
        return elem.text.match("S\\d\\d?") != null;
    }

    /**
     * Converts string representation of a date into its long value
     * @param {Object} tweet
     */
    function mapDate(tweet) {
        tweet.created_at = Date.parse(tweet.created_at);
        return tweet;
    }

    function onBeforeLoad() {
        $("#allTweets").fadeOut();
        $("#filteredTweets").fadeOut();
        if (typeof ($.mobile) !== 'undefined') {
            $.mobile.loading('show', {
                text:'Loading tweets',
                textVisible:true,
                theme:'b',
                html:""
            });
        }
    }

    /**
     * Callback trigger after loading of all tweet sources is finished.
     */
    function onLoadFinished() {
        logger.debug("onLoadFinished(): Loading finished!");
        if (typeof ($.mobile) !== 'undefined') {
            $.mobile.loading('hide');
        }
        $("#allTweets").fadeIn();
        $("#filteredTweets").fadeIn();
    }

    //-----------------------------------------------------------------------
    // scoped methods definitions
    //-----------------------------------------------------------------------

    /**
     * Refresh function
     */
    $scope.refresh = function () {
        logger.debug("update triggered");
        $scope.tweets = [];
        $scope.filteredTweets = [];
        load();
    }
};

